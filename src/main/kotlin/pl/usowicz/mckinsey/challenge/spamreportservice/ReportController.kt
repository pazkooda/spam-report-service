package pl.usowicz.mckinsey.challenge.spamreportservice

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("reports")
class ReportController(private val reportRepository: ReportRepository) {

    @PostMapping
    fun saveReport(reportEntry: ReportEntry): String {
        return reportRepository.save(reportEntry)
    }

    @GetMapping("{id}")
    fun find(@PathVariable id: String): ReportEntry {
        return reportRepository.find(id) ?: throw ResourceNotFoundException()
    }

    @GetMapping
    fun findAll(@RequestParam("unresolved") unresolved: Boolean = false): Collection<ReportEntry> {
        return if (unresolved)
            reportRepository.findUnresolved()
        else
            reportRepository.findAll()
    }

    @PutMapping("{id}")
    fun resolve(@PathVariable id: String, @RequestBody body: ResolveRequest) {
        if (body.ticketState == ReportEntry.CLOSED) {
            val entry = reportRepository.find(id)
            entry?.let { reportRepository.update(it.resolve()) } ?: throw ResourceNotFoundException()
        }
    }

    @PostMapping("{id}/block")
    fun block(@PathVariable id: String) {
        //since there is no clear definition what "Blocking" mean in this system I've made simples assumption that
        //until CLOSED state for this entry will be marking it as BLOCKING.
        // In real world I'd ask Product Owner how to store blocking information
        val entry = reportRepository.find(id)
        entry?.let { reportRepository.update(it.block()) } ?: throw ResourceNotFoundException()
    }

}

data class ResolveRequest(val ticketState: String)

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No report entry found")
class ResourceNotFoundException : RuntimeException()

