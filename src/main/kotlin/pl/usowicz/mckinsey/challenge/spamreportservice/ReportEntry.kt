package pl.usowicz.mckinsey.challenge.spamreportservice

data class ReportEntry(var id: String,
                       var source: String?,
                       var sourceIdentityId: String?,
                       var reference: Reference,
                       var state: String? = ReportEntry.OPEN,
                       var payload: Payload,
                       var created: String?) { //in real world should be of type OffsetDateTime with proper Jackson JavaTime module

    fun resolve(): ReportEntry {
        return this.copy(state = CLOSED)
    }

    fun block(): ReportEntry {
        return this.copy(state = BLOCKING)
    }

    companion object {
        //most likely should be an enum with proper Jackson Mapper - no time for this now :(
        const val OPEN = "OPEN"
        const val BLOCKING = "BLOCKING"
        const val CLOSED = "CLOSED"
    }

}

data class Payload(var source: String?,
                   var reportType: String?,
                   var message: String?,
                   var reportId: String?,
                   var referenceResourceId: String?,
                   var referenceResourceType: String?)

data class Reference(var referenceId: String?,
                     var referenceType: String?)
