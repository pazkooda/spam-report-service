package pl.usowicz.mckinsey.challenge.spamreportservice

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
class ReportRepository {

    companion object {
        //sorry! no time for real DB in 3 hours time
        val IN_MEMORY_DB: MutableMap<String, ReportEntry> = LinkedHashMap()
    }

    @PostConstruct
    private fun postConstruct() {
        fillInitialData()
    }

    fun save(reportEntry : ReportEntry) : String {
        val id = reportEntry.id
        IN_MEMORY_DB[id] = reportEntry
        return id
    }

    fun find(id: String) : ReportEntry? {
        return IN_MEMORY_DB[id]
    }

    fun findUnresolved(): Collection<ReportEntry> {
        return IN_MEMORY_DB.values.filter { it.state != ReportEntry.CLOSED }
    }

    fun findAll(): Collection<ReportEntry> {
        return IN_MEMORY_DB.values
    }

    private fun fillInitialData() {
        val dataAsString = this::class.java.classLoader.getResource("data.json").readText()
        val mapper = jacksonObjectMapper()
        val initialData: List<ReportEntry> = mapper.readValue(dataAsString)
        initialData.forEach { save(it) }
    }

    fun update(updateEntry: ReportEntry) {
        IN_MEMORY_DB[updateEntry.id] = updateEntry
    }

}
