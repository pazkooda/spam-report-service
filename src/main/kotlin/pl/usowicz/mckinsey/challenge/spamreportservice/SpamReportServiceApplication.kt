package pl.usowicz.mckinsey.challenge.spamreportservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpamReportServiceApplication

fun main(args: Array<String>) {
	runApplication<SpamReportServiceApplication>(*args)
}
